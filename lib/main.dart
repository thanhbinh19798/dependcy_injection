import 'package:dependency/get_it.dart';
import 'package:flutter/material.dart';

import 'model/plan.dart';

void main() {
  setupLocator();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    var plane = getIt<Plane>();

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
                onPressed: () {
                  plane.setPlane();
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content:
                          Text("${plane.getSpeed()} state ${plane.getFly()}")));
                },
                child: Text("Click")),
            ElevatedButton(
                onPressed: () {
                  plane.changeFlying("Super Sonic");
                  plane.changeSpeed(1000);
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content:
                          Text("${plane.getSpeed()} state ${plane.getFly()}")));
                },
                child: Text("Update"))
          ],
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
