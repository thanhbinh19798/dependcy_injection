import 'package:dependency/model/flying.dart';
import 'package:dependency/model/plan.dart';
import 'package:dependency/model/running.dart';
import 'package:get_it/get_it.dart';

GetIt getIt = GetIt.instance;

void setupLocator() {
  getIt.registerLazySingleton(() => Plane(flying: getIt.get(), running: getIt.get()));
  getIt.registerLazySingleton(() => Flying());
  getIt.registerLazySingleton(() => Running());
}