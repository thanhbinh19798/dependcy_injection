
import 'package:dependency/model/running.dart';

import 'flying.dart';

class Plane {
    late Flying flying;
    late Running running;

    void changeSpeed(int speeding) {
      running.speed = speeding;
    }

    void changeFlying(String moving) {
      flying.move = moving;
    }

    void setPlane() {
      int speeding = 500;
      String moving = "TAKE_OFF";
      running.speed = speeding;
      flying.move = moving;
    }

    int getSpeed() => running.speed;

    String getFly() => flying.move;

    Plane({required this.flying, required this.running});
}